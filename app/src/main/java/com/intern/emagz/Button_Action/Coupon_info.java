package com.intern.emagz.Button_Action;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Coupon_info {
    @SerializedName("Validity")
    @Expose
    private String validity;
    @SerializedName("Amount_Discount")
    @Expose
    private String amountDiscount;
    @SerializedName("Applicable")
    @Expose
    private String applicable;
    @SerializedName("coupon_code")
    @Expose
    private String couponCode;

    public String getValidity() {
        return validity;
    }

    public String getAmountDiscount() {
        return amountDiscount;
    }

    public String getApplicable() {
        return applicable;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public void setAmountDiscount(String amountDiscount) {
        this.amountDiscount = amountDiscount;
    }

    public void setApplicable(String applicable) {
        this.applicable = applicable;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }
}

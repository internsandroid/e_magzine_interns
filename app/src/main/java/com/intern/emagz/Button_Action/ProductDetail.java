package com.intern.emagz.Button_Action;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetail {
    @SerializedName("Owner_name")
    @Expose
    private String ownerName;
    @SerializedName("Release_date")
    @Expose
    private String releaseDate;
    @SerializedName("Price")
    @Expose
    private String price;
    @SerializedName("Contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("Email_id")
    @Expose
    private String emailId;
    @SerializedName("Website_link")
    @Expose
    private String websiteLink;

    public String getOwnerName() {
        return ownerName;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getPrice() {
        return price;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public String getAddress() {
        return address;
    }

    public String getEmailId() {
        return emailId;
    }

    public String getWebsiteLink() {
        return websiteLink;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public void setWebsiteLink(String websiteLink) {
        this.websiteLink = websiteLink;
    }
}

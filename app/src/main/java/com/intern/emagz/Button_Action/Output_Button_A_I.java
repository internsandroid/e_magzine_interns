package com.intern.emagz.Button_Action;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Output_Button_A_I {
    @SerializedName("responseStatus")
    @Expose
    private Integer responseStatus;
    @SerializedName("Product_details")
    @Expose
    private List<ProductDetail> productDetails = null;

    public Integer getResponseStatus() {
        return responseStatus;
    }

    public List<ProductDetail> getProductDetails() {
        return productDetails;
    }

    public void setResponseStatus(Integer responseStatus) {
        this.responseStatus = responseStatus;
    }

    public void setProductDetails(List<ProductDetail> productDetails) {
        this.productDetails = productDetails;
    }
}

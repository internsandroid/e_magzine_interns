package com.intern.emagz.Button_Action;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Input_button_A {
    @SerializedName("operation")
    @Expose
    private String operation;
    @SerializedName("api_key")
    @Expose
    private String apiKey;
    @SerializedName("Magazine_id")
    @Expose
    private String magazineId;
    @SerializedName("Airline_id")
    @Expose
    private String airlineId;
    @SerializedName("Flight_id")
    @Expose
    private String flightId;
    @SerializedName("Passenger_id")
    @Expose
    private String passengerId;
    @SerializedName("User_id")
    @Expose
    private String userId;
    @SerializedName("Page_description")
    @Expose
    private String pageDescription;
    @SerializedName("Page_image")
    @Expose
    private String pageImage;
    @SerializedName("Feedback_body")
    @Expose
    private String feedbackBody;
    @SerializedName("page_number")
    @Expose
    private String pageNumber;

    public String getOperation() {
        return operation;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getMagazineId() {
        return magazineId;
    }

    public String getAirlineId() {
        return airlineId;
    }

    public String getFlightId() {
        return flightId;
    }

    public String getPassengerId() {
        return passengerId;
    }

    public String getUserId() {
        return userId;
    }

    public String getFeedbackBody() {
        return feedbackBody;
    }

    public String getPageNumber() {
        return pageNumber;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public void setMagazineId(String magazineId) {
        this.magazineId = magazineId;
    }

    public void setAirlineId(String airlineId) {
        this.airlineId = airlineId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public void setPassengerId(String passengerId) {
        this.passengerId = passengerId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setFeedbackBody(String feedbackBody) {
        this.feedbackBody = feedbackBody;
    }

    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setPageDescription(String pageDescription) {
        this.pageDescription = pageDescription;
    }

    public void setPageImage(String pageImage) {
        this.pageImage = pageImage;
    }

    public String getPageDescription() {
        return pageDescription;
    }

    public String getPageImage() {
        return pageImage;
    }
}

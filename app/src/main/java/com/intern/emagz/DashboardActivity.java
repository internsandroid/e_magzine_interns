package com.intern.emagz;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.intern.emagz.AirlineList.AirlineList;
import com.intern.emagz.AirlineList.Input_Airlinelist;
import com.intern.emagz.AirlineList.Output_Airlinelist;
import com.intern.emagz.Flightlist.FlightList;
import com.intern.emagz.Flightlist.Input_FlightList;
import com.intern.emagz.Flightlist.Output_FlightList;
import com.intern.emagz.Login.loginActivity;
import com.intern.emagz.Magzine_list.Adapter_Magzine_L;
import com.intern.emagz.Magzine_list.Input_Magzine_L;
import com.intern.emagz.Magzine_list.Magzinelist;
import com.intern.emagz.Magzine_list.Output_Magzine_L;
import com.intern.emagz.ShowProfile.Userdetail;
import com.intern.emagz.api.Api;
import com.intern.emagz.api.ApiClient;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends AppCompatActivity {
    List<AirlineList> airlineinfo;
    List<FlightList> flightinfo;
    public String[] airlinename;
    Spinner airlinespinner, flightspinner;
    public String[] flightcode;
    public String[] flightid;
    LinearLayout linearLayout;
    public String[] airlineid;
    public static  String tempairid;
   public static  String tempflightid;
    RecyclerView recyclerView;
    public String[] magzineimg;
    List<Magzinelist> magzinelist;
    public String[] magdesc;
    public String[] magmonth;
    public String[] magtitle;
    public String[] magid;
    public byte[] byteimg;
    public ArrayList<Bitmap> bitimg = new ArrayList<>();
    MyDBHelper db;
    ArrayList<String> list = new ArrayList<>();
    ProgressBar progressBar;
    SharedPreferences preferences;
    CommonFuntion cf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        linearLayout = findViewById(R.id.flightview);
        airlinespinner = findViewById(R.id.airlinelist);
        recyclerView = findViewById(R.id.recycleview);
        progressBar = findViewById(R.id.progressBar);
        flightspinner = findViewById(R.id.flightlist);
        db = new MyDBHelper(DashboardActivity.this);
        Cursor res = db.getairlinedata();

        if(cf.isConnected(this)){  //ONLINE MODE
           airlinelist();
        }else {           // OFFLINE MODE
            int size =  res.getCount();
            if( size > 0)
            {
                int i=0,j=0;
                airlinename = new String[size];
                airlineid  = new String[size];
                while (res.moveToNext())
                {
                    airlinename[i]= res.getString(1).toString();
                    airlineid[i]= res.getString(0).toString();
                    i++;
                }
                Log.i("size", String.valueOf(airlinename.length));
                ArrayAdapter arrayAdapter = new ArrayAdapter(DashboardActivity.this, android.R.layout.simple_spinner_item,airlinename);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                airlinespinner.setAdapter(arrayAdapter);


                Cursor res2 =db.getflightdata();
                int size2 = res2.getCount();
                flightcode = new String[size2];
                flightid = new String[size2];

                if(size2 > 0)
                {
                    while (res2.moveToNext())
                    {
                        flightid[j]= res2.getString(0).toString();
                        flightcode[j]= res2.getString(1).toString();
                        j++;
                    }
                    Log.i("size2", String.valueOf(flightcode.length));
                    ArrayAdapter arrayAdapter2 = new ArrayAdapter(DashboardActivity.this, android.R.layout.simple_spinner_item,flightcode);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    flightspinner.setAdapter(arrayAdapter2);

                    Cursor res3 = db.getMagazinelistdata();
                    int size3 = res3.getCount();
                    if(size3 > 0){
                        int magsize = res3.getCount();
                        magdesc = new String[magsize];
                        magmonth = new String[magsize];
                        magtitle = new String[magsize];
                        magzineimg =  new String[magsize];
                        magid = new String[magsize];
                        int k=0;
                        while (res3.moveToNext())
                        {
                            magdesc[k] = res3.getString(5).toString();
                            magmonth[k] =  res3.getString(6).toString();
                            magtitle[k] =  res3.getString(4).toString();
                            //   magzineimg[j] =   res3.getString(3).toString();
                            byteToimg(res3.getBlob(3));
                            magid[k] =  res3.getString(2).toString();
                            k++;
                        }
                        Featurecontroller.getInstance().setMagazinelist(magid,magtitle,magdesc,magmonth,bitimg);
                        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),2);
                        recyclerView.setLayoutManager(gridLayoutManager);
                        recyclerView.setAdapter(new Adapter_Magzine_L(Featurecontroller.getInstance().getMagazineimg(),Featurecontroller.getInstance().getMagdesc(),Featurecontroller.getInstance().getMagmonth(), Featurecontroller.getInstance().getMagtitle(),DashboardActivity.this));
                        recyclerView.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }

            } else{
                airlinelist();
            }

        }





        airlinespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tempairid = airlineid[position];
                Featurecontroller.getInstance().setAirlineid(tempairid);
                Log.i("airid and airname", airlineid[position].toString()+" "+airlinespinner.getSelectedItem().toString());
                FlightList(airlineid[position].toString());
                //linearLayout.setVisibility(View.VISIBLE);
             //   recyclerView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        flightspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               tempflightid = flightid[position];
               Featurecontroller.getInstance().setFlightid(tempflightid);
                Log.i("f_code and f_id",flightcode[position].toString()+ " " +flightid[position].toString());
                magzinelist(tempairid,tempflightid);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.profile_show:
                Toast.makeText(getApplicationContext(), "show profile selected", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(DashboardActivity.this, Userdetail.class);
                startActivity(i);
                return true;

            case  R.id.Logs:
                Toast.makeText(getApplicationContext(), "Logs selected", Toast.LENGTH_SHORT).show();
                Intent ii = new Intent(DashboardActivity.this, Logs.class);
                startActivity(ii);
                return true;
            case R.id.Logout:
                // Logout
                Toast.makeText(getApplicationContext(), "Logout selected", Toast.LENGTH_SHORT).show();
                preferences= getApplicationContext().getSharedPreferences("Credentials",0);
                SharedPreferences.Editor editor = preferences.edit();
                editor.remove("username");
                editor.remove("password");
                editor.commit();
                Intent intent = new Intent(DashboardActivity.this, loginActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void airlinelist() {

            Input_Airlinelist i = new Input_Airlinelist();
            i.setApiKey("cda11aoip2Ry07CGWmjEqYvPguMZTkBel1V8c3XKIxwA6zQt5s");
            i.setOperation("Airline_list");

            Api api = ApiClient.getClient().create(Api.class);
            Call<Output_Airlinelist> call = api.getAirlinelist(i);
            call.enqueue(new Callback<Output_Airlinelist>() {
                @Override
                public void onResponse(Call<Output_Airlinelist> call, Response<Output_Airlinelist> response) {
                    if (response.body() != null) {
                        if (response.body().getResponseStatus() == 200) {
                            airlineinfo = response.body().getAirlineList();
                            int size = airlineinfo.size();
                            airlinename = new String[size];
                            airlineid = new String[size];

                            for (int i = 0; i < size; i++) {
                                airlinename[i] = airlineinfo.get(i).getName().toString();
                                airlineid[i] = airlineinfo.get(i).getId().toString();
                                db.insertairlinedata(airlineinfo.get(i).getId().toString(), airlineinfo.get(i).getName().toString());
                            }
                            //db.ShowRecords();
                            ArrayAdapter arrayAdapter = new ArrayAdapter(DashboardActivity.this, android.R.layout.simple_spinner_item, airlinename);
                            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            airlinespinner.setAdapter(arrayAdapter);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Output_Airlinelist> call, Throwable t) {

                }
            });


    }

    public void FlightList(final String code) {
        Input_FlightList i = new Input_FlightList();
        i.setOperation("Flight_list");
        i.setApiKey("cda11aoip2Ry07CGWmjEqYvPguMZTkBel1V8c3XKIxwA6zQt5s");
        i.setAirlineCode(code.toString());
        Api api = ApiClient.getClient().create(Api.class);
        Call<Output_FlightList> call = api.flightlist(i);
        call.enqueue(new Callback<Output_FlightList>() {
            @Override
            public void onResponse(Call<Output_FlightList> call, Response<Output_FlightList> response) {
                if (response.body().getResponseStatus() == 200) {
                    flightinfo = response.body().getFlightList();
                    int size = flightinfo.size();
                    flightcode = new String[size];
                    flightid = new String[size];

                    for (int i = 0; i < size; i++) {
                        flightcode[i] = flightinfo.get(i).getFlightCode().toString();
                        flightid[i] = flightinfo.get(i).getId().toString();
                        db.insertflightdata(flightinfo.get(i).getId().toString(), flightinfo.get(i).getFlightCode().toString());
                    }

                    ArrayAdapter arrayAdapter = new ArrayAdapter(DashboardActivity.this, android.R.layout.simple_spinner_item, flightcode);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    flightspinner.setAdapter(arrayAdapter);
                } else {
                    flightspinner.setAdapter(null);
                    linearLayout.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Output_FlightList> call, Throwable t) {

            }
        });
    }

    public void magzinelist(String aid,String fid)
    {   progressBar.setVisibility(View.VISIBLE);
        Input_Magzine_L i = new Input_Magzine_L();
        i.setAirlineId(aid);
        i.setApiKey("cda11aoip2Ry07CGWmjEqYvPguMZTkBel1V8c3XKIxwA6zQt5s");
        i.setFlightId(fid);
        i.setOperation("Magazine_list");

        Api api = ApiClient.getClient().create(Api.class);
        Call<Output_Magzine_L> call = api.getMagzinelist(i);
        call.enqueue(new Callback<Output_Magzine_L>() {
            @Override
            public void onResponse(Call<Output_Magzine_L> call, Response<Output_Magzine_L> response) {

                if(response.body().getResponseStatus() == 200)
                {     progressBar.setVisibility(View.VISIBLE);
                    magzinelist =  response.body().getAirlineList();
                    int size = magzinelist.size();
                    magdesc = new String[size];
                    magmonth = new String[size];
                    magtitle = new String[size];
                    magzineimg =  new String[size];
                    magid = new String[size];

                    for(int i=0; i<size; i++)
                    {
                        magid[i] = magzinelist.get(i).getMagazineId().toString();
                        magtitle[i] = magzinelist.get(i).getTitle().toString();
                        magmonth[i] = magzinelist.get(i).getMonth().toString();
                        magdesc[i] = magzinelist.get(i).getDescription().toString();
                        magzineimg[i] = magzinelist.get(i).getMagazineImage().toString();
                        storeimg(magid[i],magtitle[i],magmonth[i],magdesc[i],magzineimg[i]);
                    }

                    recyclerView.setVisibility(View.VISIBLE);
                    GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),2);
                    recyclerView.setLayoutManager(gridLayoutManager);
                    recyclerView.setAdapter(new Adapter_Magzine_L(magzineimg,magdesc,magmonth,magtitle,DashboardActivity.this));
                    progressBar.setVisibility(View.INVISIBLE);
                }
                else if (response.body().getResponseStatus() == 204)
                {recyclerView.setVisibility(View.INVISIBLE);
                    Toast.makeText(DashboardActivity.this, "No Magzine found",Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Output_Magzine_L> call, Throwable t) {

            }
        });
    }
    public void storeimg(final String id, final String title , final String month , final String desc , String imgurl)
    {

        Glide.with(this)
                .asBitmap()
                .load(imgurl)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        resource.compress(Bitmap.CompressFormat.JPEG, 70, stream);
                        byteimg  = stream.toByteArray();
                        db.insertMagazinelist(tempairid,tempflightid,id,byteimg,title,desc,month);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });

        db.close();
        Log.i("call","Method called");


    }

    public void byteToimg(byte[] byteimg){
        bitimg.add(BitmapFactory.decodeByteArray(byteimg,0,byteimg.length));
    }



}
package com.intern.emagz.Login;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.intern.emagz.CommonFuntion;
import com.intern.emagz.DashboardActivity;
import com.intern.emagz.Featurecontroller;
import com.intern.emagz.R;
import com.intern.emagz.Registration.RegisterActivity;
import com.intern.emagz.api.Api;
import com.intern.emagz.api.ApiClient;
import com.intern.emagz.forgotpass.Input_Forgotpass;
import com.intern.emagz.forgotpass.Output_Forgotpass;
import com.intern.emagz.otp.Input_sendOTP;
import com.intern.emagz.otp.Output_sendOTP;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class loginActivity extends AppCompatActivity {
    EditText mobile,pass,otp;
    TextView forgot;
    ArrayList<UserDetail> userDetails;
    Button btn;
    ImageView i;
    String Sname,Spass;
    SharedPreferences sharedPreferences ;
    SharedPreferences.Editor editor;
    CommonFuntion cf;
    TextView vcode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
       // CheckUPdate();
        init();
        PackageInfo pinfo = null;
        try {
            pinfo = getApplication().getPackageManager().getPackageInfo(getApplication().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String name = pinfo.versionName;
        vcode.setText("Application Version : " + name);


        cf.isConnected(this);
        Sname = sharedPreferences.getString("username",null);
        Spass  = sharedPreferences.getString("password", null);


        //Shared Preference
        if(cf.isConnected(this)){
            if((Sname != null) && (Spass != null))
            {
                login(Sname,Spass);
            }
            else {
                // login first
                 }
        }else {
            if((Sname != null) && (Spass != null))
            {
                // intent pass to dashboard
                Intent i = new Intent(loginActivity.this, DashboardActivity.class);
                startActivity(i);
            }
            else{
                //Dialog check connection
            }

        }

        Intent intent = getIntent();
        String phoneno = intent.getStringExtra("phoneno");
        mobile.setText(phoneno);

       i.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent i = new Intent(loginActivity.this, RegisterActivity.class);
               startActivity(i);
           }
       });

    }


    private void CheckUPdate() {
        loginActivity.VersionChecker versionChecker = new VersionChecker();
        try
        {
            // String appVersionName = BuildConfig.VERSION_NAME;
            PackageInfo pinfo = null;
            try {
                pinfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            String name = pinfo.versionName;
            String mLatestVersionName = versionChecker.execute().get();
            Integer i=name.compareTo(mLatestVersionName);
            System.out.println(name);
            System.out.println(mLatestVersionName);
            System.out.println(i);


            if(i<0){
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(loginActivity.this);
                alertDialog.setTitle("Please update your app");
                alertDialog.setCancelable(true);
                alertDialog.setMessage("This app version is no longer supported. Please update your app from the Play Store.");
                alertDialog.setPositiveButton("UPDATE NOW", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = getPackageName();
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                });
               /* alertDialog.setNegativeButton("Later", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });*/
                alertDialog.show();
            }

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class VersionChecker extends AsyncTask<String, String, String> {
        private String newVersion;
        @Override
        protected String doInBackground(String... params) {

            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id="+getPackageName())
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select(".hAyfc .htlgb")
                        .get(7)
                        .ownText();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return newVersion;
        }
    }


    public void init()
    {
        sharedPreferences = getApplicationContext().getSharedPreferences("Credentials",0);
        editor = sharedPreferences.edit();
        mobile = findViewById(R.id.phone);
        pass =  findViewById(R.id.password);
        btn = findViewById(R.id.btn_login);
        forgot = findViewById(R.id.forgot_pass);
        otp = findViewById(R.id.otp);
        vcode = findViewById(R.id.vno_txt);
        i = findViewById(R.id.go_to_register);
    }

    public void otpLogin(View view)
    {            otp.setVisibility(View.VISIBLE);
                 pass.setText("");

        Input_sendOTP i = new Input_sendOTP();
        i.setApiKey("cda11aoip2Ry07CGWmjEqYvPguMZTkBel1V8c3XKIxwA6zQt5s");
        i.setCountryCode("+91");
        i.setEmailId("");
        i.setOperation("send_otp");
        i.setPhoneNumber(mobile.getText().toString());
        Api api = ApiClient.getClient().create(Api.class);
        Call<Output_sendOTP> call = api.getOtp(i);
        call.enqueue(new Callback<Output_sendOTP>() {
            @Override
            public void onResponse(Call<Output_sendOTP> call, Response<Output_sendOTP> response) {
                if(response.body() != null)
                {
                    if(response.body().getResponseStatus() == 200)
                    {
                        Toast.makeText(loginActivity.this,"Your Otp has sent",Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Output_sendOTP> call, Throwable t) {
                Toast.makeText(loginActivity.this," otp Unccessfull",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void Loginhere(View view)
    {    if(mobile.getText().toString().equals("") && pass.getText().toString().equals(""))
        {
          Toast.makeText(this,"Enter username and password",Toast.LENGTH_SHORT).show();
        }else if(mobile.getText().toString().equals("") )
              {
              Toast.makeText(this,"Enter username",Toast.LENGTH_SHORT).show();
              }else  if(pass.getText().toString().equals(""))
              {
                  Toast.makeText(this,"Enter Password",Toast.LENGTH_SHORT).show();
              }else{
                 login(mobile.getText().toString(),pass.getText().toString());
              }

    }

    public  void login(final String namee, final String passs)
    {
        InputLogin inputLogin = new InputLogin();
        inputLogin.setApiKey("cda11aoip2Ry07CGWmjEqYvPguMZTkBel1V8c3XKIxwA6zQt5s");
        inputLogin.setCountryCode("+91");
        inputLogin.setOperation("user_login");
        inputLogin.setOtp(otp.getText().toString());
        inputLogin.setPassword(passs);
        inputLogin.setPhone(namee);

        Api api = ApiClient.getClient().create(Api.class);
        Call<OutputLogin> call = api.getLogin(inputLogin);
        call.enqueue(new Callback<OutputLogin>() {
            @Override
            public void onResponse(Call<OutputLogin> call, Response<OutputLogin> response) {
                if(response.body() != null)
                {
                    if(response.body().getResponseStatus() == 200)
                    {
                        userDetails = (ArrayList<UserDetail>) response.body().getUserDetails();
                        editor.putString("username",namee);
                        editor.putString("password",passs);
                        editor.commit();
                        Featurecontroller.getInstance().setUserdetails(userDetails);
                        Toast.makeText(loginActivity.this," Login Successfull",Toast.LENGTH_SHORT).show();



                        Intent i = new Intent(loginActivity.this, DashboardActivity.class);
                        startActivity(i);
                    }
                }
            }

            @Override
            public void onFailure(Call<OutputLogin> call, Throwable t) {
                Toast.makeText(loginActivity.this," Login Failed",Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void forgot(View v)
    {
        Input_Forgotpass i = new Input_Forgotpass();
        i.setApiKey("cda11aoip2Ry07CGWmjEqYvPguMZTkBel1V8c3XKIxwA6zQt5s");
        i.setCountryCode("+91");
        i.setEmailId("");
        i.setOperation("forgot_password");
        i.setPhoneNumber(mobile.getText().toString());

        Api api = ApiClient.getClient().create(Api.class);
        Call<Output_Forgotpass> call = api.getpass(i);
        call.enqueue(new Callback<Output_Forgotpass>() {
            @Override
            public void onResponse(Call<Output_Forgotpass> call, Response<Output_Forgotpass> response) {
                if(response.body() != null)
                {
                    if(response.body().getResponseStatus() == 200)
                    {
                        Toast.makeText(loginActivity.this,"New password sent to your Mobile no", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Output_Forgotpass> call, Throwable t) {
                Toast.makeText(loginActivity.this," password Uuccessfull",Toast.LENGTH_SHORT).show();
            }
        });
    }

}
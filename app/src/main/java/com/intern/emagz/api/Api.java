package com.intern.emagz.api;


import com.intern.emagz.AirlineList.Input_Airlinelist;
import com.intern.emagz.AirlineList.Output_Airlinelist;
import com.intern.emagz.All_Logs.Input_Logs;
import com.intern.emagz.All_Logs.Output_Logs;
import com.intern.emagz.Button_Action.Input_button_A;
import com.intern.emagz.Button_Action.Output_Button_A_C;
import com.intern.emagz.Button_Action.Output_Button_A_F;
import com.intern.emagz.Button_Action.Output_Button_A_I;
import com.intern.emagz.Button_Action.Output_Button_A_L;
import com.intern.emagz.Flightlist.Input_FlightList;
import com.intern.emagz.Flightlist.Output_FlightList;
import com.intern.emagz.LandingAction.LandingActionInput;
import com.intern.emagz.LandingAction.LandingActionOutput;
import com.intern.emagz.Login.InputLogin;
import com.intern.emagz.Login.OutputLogin;
import com.intern.emagz.Magzine_list.Input_Magzine_L;
import com.intern.emagz.Magzine_list.Output_Magzine_L;
import com.intern.emagz.Page_List.Input_page_L;
import com.intern.emagz.Page_List.Output_page_L;
import com.intern.emagz.Registration.Inputregister;
import com.intern.emagz.Registration.OutputRegister;
import com.intern.emagz.ShowProfile.Input_showProfile;
import com.intern.emagz.ShowProfile.Output_showProfile;
import com.intern.emagz.forgotpass.Input_Forgotpass;
import com.intern.emagz.forgotpass.Output_Forgotpass;
import com.intern.emagz.otp.Input_sendOTP;
import com.intern.emagz.otp.Output_sendOTP;
import com.intern.emagz.update.Input_updateProfile;
import com.intern.emagz.update.Output_updateProfile;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Api {
    @POST("magazine/api/landing_action")
    Call<LandingActionOutput> getAction(@Body LandingActionInput input);


    @POST("magazine/api/user_registration")
    Call<OutputRegister> getRegistered(@Body Inputregister input);

    @POST("magazine/api/user_login")
    Call<OutputLogin> getLogin(@Body InputLogin input);

    @POST("magazine/api/send_otp")
    Call<Output_sendOTP> getOtp(@Body Input_sendOTP input);

    @POST("magazine/api/forgot_password")
    Call<Output_Forgotpass> getpass(@Body Input_Forgotpass input);

    @POST("magazine/api/user_profile_show")
    Call<Output_showProfile> getUserDetails(@Body Input_showProfile input);

    @POST("magazine/api/user_profile_update")
    Call<Output_updateProfile> updateuser(@Body Input_updateProfile input);

    @POST("magazine/api/Airline_list")
    Call<Output_Airlinelist> getAirlinelist(@Body Input_Airlinelist input);

    @POST("magazine/api/Flight_list")
    Call<Output_FlightList> flightlist(@Body Input_FlightList input);

    @POST("magazine/api/Magazine_list")
    Call<Output_Magzine_L>  getMagzinelist(@Body Input_Magzine_L input);

    @POST("magazine/api/Magazine_page_list")
    Call<Output_page_L>  getPagelist(@Body Input_page_L input);

    @POST("magazine/api/like_action")
    Call<Output_Button_A_L>  getLiked(@Body Input_button_A input);

    @POST("magazine/api/Coupon_action")
    Call<Output_Button_A_C>  getCoupon(@Body Input_button_A input);

    @POST("magazine/api/Info_action")
    Call<Output_Button_A_I>  getinfo(@Body Input_button_A input);

    @POST("magazine/api/Feedback_action")
    Call<Output_Button_A_F>  getfeedback(@Body Input_button_A input);


    @POST("magazine/api/display_error_logs")
    Call<Output_Logs>  getalllog(@Body Input_Logs input);



}

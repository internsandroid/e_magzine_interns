package com.intern.emagz.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

public static Retrofit retrofit = null;


public static Retrofit getClient()
{   String baseurl = "http://testemagazine.startupworld.in/";

    if(retrofit == null)
    {
        retrofit = new Retrofit.Builder()
                .baseUrl(baseurl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }
    return retrofit;
}
}

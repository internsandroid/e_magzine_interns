package com.intern.emagz;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.intern.emagz.Login.loginActivity;
import com.intern.emagz.Page_List.Input_page_L;
import com.intern.emagz.Page_List.Output_page_L;
import com.intern.emagz.Page_List.Page_list;
import com.intern.emagz.api.Api;
import com.intern.emagz.api.ApiClient;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Magazine_pages extends AppCompatActivity {
    List<Page_list> pageinfo= null;
    String pageimg[];
    String pageno[];
    public ArrayList<Bitmap> bitimg = new ArrayList<>();
    String magid;
    byte[] byteimg;
    RecyclerView recyclerView;
    MyDBHelper db;
    final String a_id = Featurecontroller.getInstance().airlineid.toString();
    final String f_id = Featurecontroller.getInstance().flightid.toString();
    BottomNavigationView  bottomNavigationView ;
    CommonFuntion cf;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_magzine_pages);
        db = new MyDBHelper(this);
        recyclerView = findViewById(R.id.showpages);
        bottomNavigationView = findViewById(R.id.Navigation);
        final Intent intent =getIntent();
        magid = intent.getStringExtra("Magzineid");

        if(cf.isConnected(this)){
          pagelist();
        }else {
            Cursor res = db.getMagazinepagesdata(magid);
            int size =res.getCount();
            if(size>0)
            {
                while (res.moveToNext()) {
                    byteToimg(res.getBlob(4));
                }
                recyclerView.setLayoutManager(new LinearLayoutManager(Magazine_pages.this, LinearLayoutManager.HORIZONTAL,false));
                recyclerView.setHasFixedSize(false);
                Adapter_show_pages adapter_show_pages=new Adapter_show_pages(bitimg, Magazine_pages.this);
                recyclerView.setAdapter(adapter_show_pages);
            }
            else{
                Toast.makeText(this,"No magazine Found",Toast.LENGTH_SHORT).show();
            }

        }


     //  pagelist();

    }
    private void pagelist()
    {
        Input_page_L i = new Input_page_L();
        i.setApiKey("cda11aoip2Ry07CGWmjEqYvPguMZTkBel1V8c3XKIxwA6zQt5s");
        i.setMagazineId(magid.toString());
        i.setOperation("Magazine_page_list");
        Api api = ApiClient.getClient().create(Api.class);
        Call<Output_page_L> call = api.getPagelist(i);
        call.enqueue(new Callback<Output_page_L>() {
            @Override
            public void onResponse(Call<Output_page_L> call, Response<Output_page_L> response) {

                if(response.body().getResponseStatus() == 200)
                {
                  pageinfo = response.body().getPageList();
                  int size =pageinfo.size();
                  pageimg = new String[size];
                  pageno = new String[size];
                  for(int i =0;i<size;i++)
                  {
                      pageimg[i] = pageinfo.get(i).getMagazinePageImage();
                      pageno[i] = pageinfo.get(i).getMagazinePageNumber();
                      storeimg(pageno[i],pageimg[i]);
                  }

                  //  LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(),RecyclerView.HORIZONTAL,false);
                    recyclerView.setLayoutManager(new LinearLayoutManager(Magazine_pages.this, LinearLayoutManager.HORIZONTAL,false));
                    recyclerView.setHasFixedSize(false);
                    Adapter_show_pages adapter_show_pages=new Adapter_show_pages(pageimg, Magazine_pages.this);
                    recyclerView.setAdapter(adapter_show_pages);
                    //recyclerView.setAdapter(new Adapter_show_pages(pageimg,Magzine_pages.this));
                }
            }

            @Override
            public void onFailure(Call<Output_page_L> call, Throwable t) {

            }
        });


    }

    public void storeimg( final String pno , String imgurl)
    {
        Glide.with(this)
                .asBitmap()
                .load(imgurl)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        resource.compress(Bitmap.CompressFormat.JPEG, 10, stream);
                        byteimg  = stream.toByteArray();
                        db.insertMagazinePage(a_id,f_id,magid,pno,byteimg);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });

        db.close();
       Log.i("cal","Method called");

    }
    public void byteToimg(byte[] byteimg){
        bitimg.add(BitmapFactory.decodeByteArray(byteimg,0,byteimg.length));
    }


}
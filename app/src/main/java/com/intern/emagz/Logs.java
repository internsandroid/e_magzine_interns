package com.intern.emagz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.intern.emagz.All_Logs.Input_Logs;
import com.intern.emagz.All_Logs.Output_Logs;
import com.intern.emagz.All_Logs.UserLog;
import com.intern.emagz.api.Api;
import com.intern.emagz.api.ApiClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Logs extends AppCompatActivity {


    String[] logtypes= {"Like","Feedback","Coupon"};
    Spinner spinner;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logs);
        spinner = findViewById(R.id.Log_spinner);
        recyclerView= findViewById(R.id.Logs_r);
        progressBar = findViewById(R.id.progressBar2);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getApplication(), android.R.layout.simple_list_item_1,logtypes);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position)
                {
                    case 0:
                        Toast.makeText(Logs.this,"like selected",Toast.LENGTH_SHORT).show();
                        likelogs();
                     break;
                    case 1:
                        Toast.makeText(Logs.this,"feedback selected",Toast.LENGTH_SHORT).show();
                     Feedbacklogs();
                     break;
                    case 2:
                        Toast.makeText(Logs.this,"Coupon selected",Toast.LENGTH_SHORT).show();
                        Couponlogs();
                    default:
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    public void likelogs()
    {       progressBar.setVisibility(View.VISIBLE);
           recyclerView.setVisibility(View.INVISIBLE);
        Input_Logs i = new Input_Logs();
        i.setApiKey("cda11aoip2Ry07CGWmjEqYvPguMZTkBel1V8c3XKIxwA6zQt5s");
        i.setOperation(" display_error_logs");
        i.setType("Like");
        i.setUserId(Featurecontroller.getInstance().getUserinfo().get(0).getId().toString());
        Api api = ApiClient.getClient().create(Api.class);
        Call<Output_Logs> call = api.getalllog(i);
        call.enqueue(new Callback<Output_Logs>() {
            @Override
            public void onResponse(Call<Output_Logs> call, Response<Output_Logs> response) {
                if(response.body().getResponseStatus() == 200)
                {
                    List<UserLog> userLogs = response.body().getUserLogs();
                    recyclerView.setLayoutManager(new LinearLayoutManager(Logs.this));
                    recyclerView.setAdapter(new Log_Adapter(userLogs,"Like",Logs.this));
                    Log.i("Call","Logs");
                    recyclerView.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void onFailure(Call<Output_Logs> call, Throwable t) {

            }
        });

    }

    public void Feedbacklogs()
    {       progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);
        Input_Logs i = new Input_Logs();
        i.setApiKey("cda11aoip2Ry07CGWmjEqYvPguMZTkBel1V8c3XKIxwA6zQt5s");
        i.setOperation(" display_error_logs");
        i.setType("Feedback");
        i.setUserId(Featurecontroller.getInstance().getUserinfo().get(0).getId().toString());

        Api api = ApiClient.getClient().create(Api.class);
        Call<Output_Logs> call = api.getalllog(i);
        call.enqueue(new Callback<Output_Logs>() {
            @Override
            public void onResponse(Call<Output_Logs> call, Response<Output_Logs> response) {
                if(response.body().getResponseStatus() == 200)
                {
                    List<UserLog> userLogs = response.body().getUserLogs();
                    recyclerView.setLayoutManager(new LinearLayoutManager(Logs.this));
                    recyclerView.setAdapter(new Log_Adapter(userLogs ,Logs.this));
                    Log.i("Call","Logs");
                    recyclerView.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Output_Logs> call, Throwable t) {

            }
        });

    }
    public void Couponlogs()
    {       progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);
        Input_Logs i = new Input_Logs();
        i.setApiKey("cda11aoip2Ry07CGWmjEqYvPguMZTkBel1V8c3XKIxwA6zQt5s");
        i.setOperation(" display_error_logs");
        i.setType("Coupon");
        i.setUserId(Featurecontroller.getInstance().getUserinfo().get(0).getId().toString());

        Api api = ApiClient.getClient().create(Api.class);
        Call<Output_Logs> call = api.getalllog(i);
        call.enqueue(new Callback<Output_Logs>() {
            @Override
            public void onResponse(Call<Output_Logs> call, Response<Output_Logs> response) {
                if(response.body().getResponseStatus() == 200)
                {
                    List<UserLog> userLogs = response.body().getUserLogs();

                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(Logs.this));
                    recyclerView.setAdapter(new Log_Adapter(userLogs,"Coupon",Logs.this));
                    recyclerView.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                    Log.i("Log","called");
                }
            }

            @Override
            public void onFailure(Call<Output_Logs> call, Throwable t) {

            }
        });


    }
}
package com.intern.emagz;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.intern.emagz.All_Logs.UserLog;

import java.util.List;

public class Log_Adapter extends RecyclerView.Adapter<Log_Adapter.ViewHolder> {
   String s = "";
   Context context;
    List<UserLog> logs;
    public Log_Adapter(List<UserLog> userLogs, String s,Context context) {
        this.logs = userLogs;
        this.context = context;
        this.s = s;
    }
    public Log_Adapter(List<UserLog> userLogs, Context context) {
        this.logs = userLogs;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(s.equals(""))
        {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.logs_feed_view,parent,false);
            return new ViewHolder(view);
        }
        else{
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.logs_l_c_view,parent,false);
            return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

     if(s.equals("")){
         holder.magid.setText(logs.get(position).getMagazineId().toString());
         holder.p_no.setText(logs.get(position).getPageNumber().toString());
         holder.flightid.setText(logs.get(position).getFlightId().toString());
         holder.feeds.setText(logs.get(position).getFeedbackBody().toString());
         holder.airid.setText(logs.get(position).getAirlineId().toString());
         holder.p_dis.setText(logs.get(position).getPageDescription().toString());
         Glide.with(context)
                 .load(logs.get(position).getPageImage())
                // .load("http://testemagazine.startupworld.in/magazine/magazine_page/Hello6e_nov_2020/9.jpg")
                 .placeholder(R.drawable.loading)
                 .into(holder.p_img);
     }else
     {
         holder.magid.setText(logs.get(position).getMagazineId().toString());
         holder.p_no.setText(logs.get(position).getPageNumber().toString());
         holder.flightid.setText(logs.get(position).getFlightId().toString());
         holder.airid.setText(logs.get(position).getAirlineId().toString());
         holder.p_dis.setText(logs.get(position).getPageDescription().toString());
         Glide.with(context)
                 .load(logs.get(position).getPageImage())
                // .load("http://testemagazine.startupworld.in/magazine/magazine_page/Hello6e_nov_2020/9.jpg")
                 .placeholder(R.drawable.loading)
                 .into(holder.p_img);
     }

    }

    @Override
    public int getItemCount() {
        if(s.equals(""))
        {
            return logs.size();
        }
        else{
            return logs.size();
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder{
      TextView magid,airid,flightid,feeds,p_no,p_dis;
      ImageView p_img;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            magid = itemView.findViewById(R.id.Log_magid);
            feeds= itemView.findViewById(R.id.Log_feedback);
            airid = itemView.findViewById(R.id.Log_airid);
            flightid = itemView.findViewById(R.id.Log_flightid);
            p_no= itemView.findViewById(R.id.Log_p_no);
            p_dis = itemView.findViewById(R.id.page_desc);
            p_img = itemView.findViewById(R.id.thumbnail);
        }
    }
}

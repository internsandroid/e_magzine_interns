package com.intern.emagz;

import android.content.Context;
import android.graphics.Bitmap;

import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.intern.emagz.Button_Action.Coupon_info;
import com.intern.emagz.Button_Action.Input_button_A;
import com.intern.emagz.Button_Action.Output_Button_A_C;
import com.intern.emagz.Button_Action.Output_Button_A_F;
import com.intern.emagz.Button_Action.Output_Button_A_I;
import com.intern.emagz.Button_Action.Output_Button_A_L;
import com.intern.emagz.Button_Action.ProductDetail;
import com.intern.emagz.LandingAction.LandingActionInput;
import com.intern.emagz.LandingAction.LandingActionOutput;
import com.intern.emagz.ShowProfile.Userdetail;
import com.intern.emagz.api.Api;
import com.intern.emagz.api.ApiClient;

import java.io.ByteArrayOutputStream;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public  class CommonFuntion {


    public static void liked(final Context context ,String p_no, String imgbase64)
    {
        Input_button_A i = new Input_button_A();
        i.setOperation("like_action");
        i.setAirlineId(Featurecontroller.getInstance().airlineid.toString());
        i.setApiKey("cda11aoip2Ry07CGWmjEqYvPguMZTkBel1V8c3XKIxwA6zQt5s");
        i.setFlightId(Featurecontroller.getInstance().flightid.toString());
        i.setMagazineId(Featurecontroller.getInstance().getMagid().toString());
        i.setPageNumber(p_no);
        i.setPageDescription("");
        i.setPageImage(imgbase64);
        i.setUserId(Featurecontroller.getInstance().getUserinfo().get(0).getId());
        i.setPassengerId(Featurecontroller.getInstance().getUserinfo().get(0).getPhone());
        Api api = ApiClient.getClient().create(Api.class);
        Call<Output_Button_A_L> call = api.getLiked(i);
        call.enqueue(new Callback<Output_Button_A_L>() {
            @Override
            public void onResponse(Call<Output_Button_A_L> call, Response<Output_Button_A_L> response) {
                if(response.body().getResponseStatus().equals(200))
                {
                    Toast.makeText(context,"Liked clicked new",Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Output_Button_A_L> call, Throwable t) {

            }
        });
    }

    public static void feedback(final Context context , String feedback ,String p_no, String imgbase64)
    {
        Input_button_A i = new Input_button_A();
        i.setOperation("Feedback_action");
        i.setAirlineId(Featurecontroller.getInstance().airlineid.toString());
        i.setApiKey("cda11aoip2Ry07CGWmjEqYvPguMZTkBel1V8c3XKIxwA6zQt5s");
        i.setFlightId(Featurecontroller.getInstance().flightid.toString());
        i.setMagazineId(Featurecontroller.getInstance().getMagid().toString());
        i.setPageNumber(p_no);
        i.setPageImage(imgbase64);
        i.setPageDescription("g for genius");
        i.setUserId(Featurecontroller.getInstance().getUserinfo().get(0).getId());
        i.setPassengerId(Featurecontroller.getInstance().getUserinfo().get(0).getPhone());
        i.setFeedbackBody(feedback);
        Api api = ApiClient.getClient().create(Api.class);
        Call<Output_Button_A_F> call = api.getfeedback(i);
        call.enqueue(new Callback<Output_Button_A_F>() {
            @Override
            public void onResponse(Call<Output_Button_A_F> call, Response<Output_Button_A_F> response) {
                if(response.body().getResponseStatus().equals(200))
                {
                    Toast.makeText(context,"Feedback sent",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Output_Button_A_F> call, Throwable t) {

            }
        });

    }

    public static void info(final Context context , String p_no){
        Input_button_A i = new Input_button_A();
        i.setOperation("Info_action");
        i.setAirlineId(Featurecontroller.getInstance().airlineid.toString());
        i.setApiKey("cda11aoip2Ry07CGWmjEqYvPguMZTkBel1V8c3XKIxwA6zQt5s");
        i.setFlightId(Featurecontroller.getInstance().flightid.toString());
        i.setMagazineId(Featurecontroller.getInstance().getMagid().toString());
        i.setUserId(Featurecontroller.getInstance().getUserinfo().get(0).getId());
        i.setPageNumber(p_no);
        i.setPassengerId(Featurecontroller.getInstance().getUserinfo().get(0).getPhone());
        Api api = ApiClient.getClient().create(Api.class);
        Call<Output_Button_A_I> call = api.getinfo(i);
        call.enqueue(new Callback<Output_Button_A_I>() {
            @Override
            public void onResponse(Call<Output_Button_A_I> call, Response<Output_Button_A_I> response) {
                if(response.body().getResponseStatus().equals(200))
                {
                    List<ProductDetail> info = response.body().getProductDetails();
                    Toast.makeText(context,"Info get",Toast.LENGTH_SHORT).show();
                    Featurecontroller.getInstance().setMagazineinfo(info);
                }
                else  if(response.body().getResponseStatus().equals(208))
                {
                    Toast.makeText(context,"Information not available",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Output_Button_A_I> call, Throwable t) {

            }
        });
    }

    public static void coupon(final Context context, String pageno , String imgbase64)
    {     Input_button_A i = new Input_button_A();
        i.setOperation("Coupon_action");
        i.setAirlineId(Featurecontroller.getInstance().airlineid.toString());
        i.setApiKey("cda11aoip2Ry07CGWmjEqYvPguMZTkBel1V8c3XKIxwA6zQt5s");
        i.setFlightId(Featurecontroller.getInstance().flightid.toString());
        i.setMagazineId(Featurecontroller.getInstance().getMagid().toString());
        i.setUserId(Featurecontroller.getInstance().getUserinfo().get(0).getId());
        i.setPassengerId(Featurecontroller.getInstance().getUserinfo().get(0).getPhone());
        i.setPageNumber(pageno);
        i.setPageImage(imgbase64);
        i.setPageDescription("Discription of page  Magazine "+Featurecontroller.getInstance().getMagid().toString());
        Api api = ApiClient.getClient().create(Api.class);
        Call<Output_Button_A_C> call = api.getCoupon(i);
        call.enqueue(new Callback<Output_Button_A_C>() {
            @Override
            public void onResponse(Call<Output_Button_A_C> call, Response<Output_Button_A_C> response) {
                if(response.body().getResponseStatus().equals(200))
                {
                    List<Coupon_info> info = response.body().getProductDetails();
                    Toast.makeText(context,"Coupon get",Toast.LENGTH_SHORT).show();
                    Featurecontroller.getInstance().setCouponinfo(info);
                }
                else  if(response.body().getResponseStatus().equals(208))
                {
                    Toast.makeText(context,"Coupon not Available",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Output_Button_A_C> call, Throwable t) {

            }
        });

    }

    public static  String imgtobase64(String imgurl,Context context)
    {    final Userdetail userdetail = new Userdetail();
        final String[] base64 = {""};
        Glide.with(context)
                .asBitmap()
                .load(imgurl)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                      base64[0] =  userdetail.getBase64(resource).toString();
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });

      return base64[0];
    }

    public static boolean isConnected(Context context)
    {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiinfo = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileinfo   = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if((wifiinfo != null && wifiinfo.isConnected()) || (mobileinfo != null && mobileinfo.isConnected()))
        {
            return  true;
        }else
        {
            return false;
        }
    }

}

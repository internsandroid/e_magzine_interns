package com.intern.emagz.Magzine_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Input_Magzine_L {
    @SerializedName("operation")
    @Expose
    private String operation;
    @SerializedName("api_key")
    @Expose
    private String apiKey;
    @SerializedName("airline_id")
    @Expose
    private String airlineId;
    @SerializedName("flight_id")
    @Expose
    private String flightId;

    public String getOperation() {
        return operation;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getAirlineId() {
        return airlineId;
    }

    public String getFlightId() {
        return flightId;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public void setAirlineId(String airlineId) {
        this.airlineId = airlineId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }
}

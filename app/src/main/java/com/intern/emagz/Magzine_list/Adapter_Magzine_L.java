package com.intern.emagz.Magzine_list;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.intern.emagz.Featurecontroller;
import com.intern.emagz.Magazine_pages;
import com.intern.emagz.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Adapter_Magzine_L extends RecyclerView.Adapter<Adapter_Magzine_L.Viewholder> {
    private String[] magimgg;
    private String[] magmonthh;
    private String[] magdescc;
    private String[] magtitlee;
    Context context;
    ArrayList<Bitmap> storedimg = new ArrayList<>();
    int i =0;

    public Adapter_Magzine_L(String[] magzineimg, String[] magdesc, String[] magmonth, String[] magtitle, Context context) {
        i=0;
        this.magdescc = magdesc;
        this.magimgg = magzineimg;
        this.magmonthh = magmonth;
        this.magtitlee =magtitle;
        this.context = context;
    }
    public Adapter_Magzine_L(ArrayList<Bitmap> img, String[] magdesc, String[] magmonth, String[] magtitle, Context context) {
         i=1;
        this.magdescc = magdesc;
        this.storedimg = img;
        this.magmonthh = magmonth;
        this.magtitlee =magtitle;
        this.context = context;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.magazine_l_view,parent,false);
        return new Viewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final Viewholder holder, final int position) {
//        MyDBHelper db;
//        db = new MyDBHelper(context);
//        int size = db.getMagazinelistdata().getCount();
        if(i == 0)
        {
            holder.month.setText(magmonthh[position]);
            holder.title.setText(magtitlee[position]);
            holder.desc.setText(magdescc[position]);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, Magazine_pages.class);
                    intent.putExtra("Magzineid",String.valueOf(position+1));
                    Featurecontroller.getInstance().setMagid(String.valueOf(position+1));
                    context.startActivity(intent);

                }
            });
            Picasso.with(context).load(magimgg[position]).resize(100, 100).
                    centerCrop().into(holder.img);
        }
        else{
            holder.month.setText(magmonthh[position]);
            holder.title.setText(magtitlee[position]);
            holder.desc.setText(magdescc[position]);
            holder.img.setImageBitmap(storedimg.get(position));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, Magazine_pages.class);
                    intent.putExtra("Magzineid",String.valueOf(position+1));
                    Featurecontroller.getInstance().setMagid(String.valueOf(position+1));
                    context.startActivity(intent);
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        if(i==1)
        {
            return  storedimg.size();
        }else
        {
            return magtitlee.length;
        }

    }


    public class Viewholder extends RecyclerView.ViewHolder {
       ImageView img;
       TextView desc,title,month;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.image);
            desc= itemView.findViewById(R.id.discription);
            title = itemView.findViewById(R.id.title);
            month = itemView.findViewById(R.id.month);
        }
    }

}

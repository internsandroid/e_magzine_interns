package com.intern.emagz.Magzine_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Magzinelist {
    @SerializedName("Magazine_Id")
    @Expose
    private String magazineId;
    @SerializedName("Magazine_Name")
    @Expose
    private String magazineName;
    @SerializedName("Magazine_Image")
    @Expose
    private String magazineImage;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("description")
    @Expose
    private String description;

    public void setMagazineId(String magazineId) {
        this.magazineId = magazineId;
    }

    public void setMagazineName(String magazineName) {
        this.magazineName = magazineName;
    }

    public void setMagazineImage(String magazineImage) {
        this.magazineImage = magazineImage;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMagazineId() {
        return magazineId;
    }

    public String getMagazineName() {
        return magazineName;
    }

    public String getMagazineImage() {
        return magazineImage;
    }

    public String getTitle() {
        return title;
    }

    public String getMonth() {
        return month;
    }

    public String getYear() {
        return year;
    }

    public String getDescription() {
        return description;
    }
}

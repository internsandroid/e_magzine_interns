package com.intern.emagz;

public class Constants {
    public static final String DB_NAME = "E_MAGAZINEEE";
    public static final int DB_VERSION = 1;
    public static final String AIRLINE_TABLE_NAME = "AIRLINE_LIST";
    public static final String FLIGHT_TABLE_NAME = "FLIGHT_LIST";
    public static final String MAGAZINE_IMG_TABLE_NAME = "MAGAZINE_IMGS";
    public static final String MAGAZINE_PAGES_TABLE_NAME = "MAGAZINE_PAGES";


    public static final String A_ID = "A_ID";
    public static final String A_NAME = "NAME";

    public static final String CREATE_AIRLINE_TABLE = "CREATE TABLE " + AIRLINE_TABLE_NAME + " ("
            + A_ID + " TEXT , "
            + A_NAME + " TEXT   "
            + ")";

    public static final String F_ID = "F_ID";
    public static final String F_CODE = "CODE";

    public static final String CREATE_FLIGHT_TABLE = "CREATE TABLE " + FLIGHT_TABLE_NAME + " ("
            + F_ID + " TEXT , "
            + F_CODE + " TEXT"
            + ")";

    public static final String M_ID = " M_ID";
    public static final String M_IMG = " M_IMG";
    public static final String M_TITLE = " M_TITLE";
    public static final String M_DESC = " M_DESC";
    public static final String M_MONTH = " M_MONTH";
    public static final String M_PNO = " M_PAGE_NO";
    public static final String M_PAGES = " M_PAGES";



    public static final String CREATE_MAGAZINE_IMG_TABLE = "CREATE TABLE " + MAGAZINE_IMG_TABLE_NAME + "("
             + A_ID + " TEXT , "
             + F_ID + " TEXT ,"
             + M_ID + " TEXT , "
             + M_IMG  + " BLOB , "
             + M_TITLE + " TEXT , "
             + M_DESC + " TEXT , "
             + M_MONTH + " TEXT "
             + ")";

    public static final String CREATE_MAGAZINE_PAGES_TABLE = "CREATE TABLE " + MAGAZINE_PAGES_TABLE_NAME + "("
            + A_ID + " TEXT , "
            + F_ID + " TEXT , "
            + M_ID + " TEXT , "
            + M_PNO + " TEXT , "
            + M_PAGES + " BLOB "
            + ")";




}

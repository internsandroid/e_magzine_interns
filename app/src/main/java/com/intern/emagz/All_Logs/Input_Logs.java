package com.intern.emagz.All_Logs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Input_Logs {@SerializedName("operation")
@Expose
private String operation;
    @SerializedName("api_key")
    @Expose
    private String apiKey;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("type")
    @Expose
    private String type;

    public String getOperation() {
        return operation;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getUserId() {
        return userId;
    }

    public String getType() {
        return type;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setType(String type) {
        this.type = type;
    }
}

package com.intern.emagz.All_Logs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserLog {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("Magazine_id")
    @Expose
    private String magazineId;
    @SerializedName("Airline_id")
    @Expose
    private String airlineId;
    @SerializedName("Flight_id")
    @Expose
    private String flightId;
    @SerializedName("Passenger_id")
    @Expose
    private String passengerId;
    @SerializedName("User_id")
    @Expose
    private String userId;
    @SerializedName("Feedback_body")
    @Expose
    private String feedbackBody;
    @SerializedName("page_number")
    @Expose
    private String pageNumber;
    @SerializedName("Page_description")
    @Expose
    private String pageDescription;
    @SerializedName("Page_image")
    @Expose
    private String pageImage;

    public String getId() {
        return id;
    }

    public String getMagazineId() {
        return magazineId;
    }

    public String getAirlineId() {
        return airlineId;
    }

    public String getFlightId() {
        return flightId;
    }

    public String getPassengerId() {
        return passengerId;
    }

    public String getUserId() {
        return userId;
    }

    public String getFeedbackBody() {
        return feedbackBody;
    }

    public String getPageNumber() {
        return pageNumber;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setMagazineId(String magazineId) {
        this.magazineId = magazineId;
    }

    public void setAirlineId(String airlineId) {
        this.airlineId = airlineId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public void setPassengerId(String passengerId) {
        this.passengerId = passengerId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setFeedbackBody(String feedbackBody) {
        this.feedbackBody = feedbackBody;
    }

    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getPageDescription() {
        return pageDescription;
    }

    public String getPageImage() {
        return pageImage;
    }

    public void setPageDescription(String pageDescription) {
        this.pageDescription = pageDescription;
    }

    public void setPageImage(String pageImage) {
        this.pageImage = pageImage;
    }
}

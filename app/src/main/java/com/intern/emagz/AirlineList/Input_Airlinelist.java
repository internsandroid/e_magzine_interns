package com.intern.emagz.AirlineList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Input_Airlinelist {

    @SerializedName("operation")
    @Expose
    private String operation;
    @SerializedName("api_key")
    @Expose
    private String apiKey;

    public String getOperation() {
        return operation;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}



package com.intern.emagz.Registration;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.intern.emagz.Featurecontroller;
import com.intern.emagz.Login.loginActivity;
import com.intern.emagz.R;
import com.intern.emagz.api.Api;
import com.intern.emagz.api.ApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    EditText name,pass,mobile,email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        init();

        ImageView imageView = (ImageView)findViewById(R.id.btn_register); //LOGIN
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Inputregister i = new Inputregister();

                i.setCountryCode("+91");
                i.setEmail(email.getText().toString());
                i.setApiKey("cda11aoip2Ry07CGWmjEqYvPguMZTkBel1V8c3XKIxwA6zQt5s");
                i.setFullName(name.getText().toString());
                i.setOperation("user_registration");
                i.setPassword(pass.getText().toString());
                i.setPhone(mobile.getText().toString());

                Api api = ApiClient.getClient().create(Api.class);
                Call<OutputRegister> call = api.getRegistered(i);
                call.enqueue(new Callback<OutputRegister>() {
                    @Override
                    public void onResponse(Call<OutputRegister> call, Response<OutputRegister> response) {
                        if(response.body()!= null)
                        {if(response.body().getResponseStatus() == 200)
                        {
                            Toast.makeText(RegisterActivity.this, name.getText().toString()+" Registered", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(RegisterActivity.this, loginActivity.class);
                            i.putExtra("phoneno",mobile.getText().toString());
                            startActivity(i);

                        }
                        }
                    }

                    @Override
                    public void onFailure(Call<OutputRegister> call, Throwable t) {
                        Toast.makeText(RegisterActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        ImageView login = findViewById(R.id.go_to_Login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegisterActivity.this, loginActivity.class);
                startActivity(i);
            }
        });
    }
    public void init()
    {
        name =findViewById(R.id.username);
        pass =findViewById(R.id.password);
        mobile = findViewById(R.id.mobileno);
        email = findViewById(R.id.email);


    }

    public void setdata()
    {
        String id= Featurecontroller.getInstance().getUserinfo().get(0).getId().toString();
    }
}
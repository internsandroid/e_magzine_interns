package com.intern.emagz;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import static com.intern.emagz.Constants.AIRLINE_TABLE_NAME;
import static com.intern.emagz.Constants.A_ID;
import static com.intern.emagz.Constants.A_NAME;
import static com.intern.emagz.Constants.FLIGHT_TABLE_NAME;
import static com.intern.emagz.Constants.F_CODE;
import static com.intern.emagz.Constants.F_ID;
import static com.intern.emagz.Constants.MAGAZINE_IMG_TABLE_NAME;
import static com.intern.emagz.Constants.MAGAZINE_PAGES_TABLE_NAME;
import static com.intern.emagz.Constants.M_DESC;
import static com.intern.emagz.Constants.M_ID;
import static com.intern.emagz.Constants.M_IMG;
import static com.intern.emagz.Constants.M_MONTH;
import static com.intern.emagz.Constants.M_PAGES;
import static com.intern.emagz.Constants.M_PNO;
import static com.intern.emagz.Constants.M_TITLE;

public class MyDBHelper extends SQLiteOpenHelper {
    public MyDBHelper(@Nullable Context context) {
        super(context, Constants.DB_NAME, null, Constants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Constants.CREATE_AIRLINE_TABLE);
        db.execSQL(Constants.CREATE_FLIGHT_TABLE);
        db.execSQL(Constants.CREATE_MAGAZINE_IMG_TABLE);
        db.execSQL(Constants.CREATE_MAGAZINE_PAGES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS " + AIRLINE_TABLE_NAME);
        onCreate(db);
        db.execSQL(" DROP TABLE IF EXISTS " + FLIGHT_TABLE_NAME);
        onCreate(db);
        db.execSQL(" DROP TABLE IF EXISTS " + MAGAZINE_IMG_TABLE_NAME);
        onCreate(db);
        db.execSQL(" DROP TABLE IF EXISTS " + MAGAZINE_PAGES_TABLE_NAME);
        onCreate(db);
    }
    public void insertairlinedata(String id, String name ){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(A_ID,id);
        values.put(A_NAME,name);
        db.insert(AIRLINE_TABLE_NAME, null, values);
        db.close();
        Log.i("air_data_S ", "Airline data store");
    }

    public void insertflightdata(String id,String code){
        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(F_ID,id);
        values.put(F_CODE,code);
        db.insert(FLIGHT_TABLE_NAME,null,values);
        db.close();
        Log.i("flight_data_S ", "Flight data store");
    }

    public  void insertMagazinelist(String A_id, String F_id, String M_id , byte[] M_img , String M_t, String M_d , String M_m){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(A_ID , A_id);
        values.put(F_ID, F_id);
        values.put(M_ID , M_id);
        values.put(M_IMG, M_img);
        values.put(M_TITLE, M_t);
        values.put(M_DESC,M_d);
        values.put(M_MONTH,M_m);
        db.insert(MAGAZINE_IMG_TABLE_NAME,null,values);
        db.close();
        Log.i("Magazine_data_S ", "Flight data store");

    }

    public void insertMagazinePage(String A_id, String F_id, String M_id , String P_NO, byte[] M_img)
    { SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(A_ID , A_id);
        values.put(F_ID, F_id);
        values.put(M_ID , M_id);
        values.put(M_PAGES, M_img);
        values.put(M_PNO,P_NO);
        db.insert(MAGAZINE_PAGES_TABLE_NAME,null,values);
        db.close();
        Log.i("Magazine_img_s","Magazine pages stored");

    }

//    public ArrayList<AirlineList> ShowRecords(){
//        ArrayList<AirlineList> modelRecords = new ArrayList<>();
//        String selectQuery = " SELECT * FROM " + Constants.AIRLINE_TABLE_NAME;
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery,null);
//        if (cursor.moveToFirst()){
//            do {
//                AirlineList airlineList = new AirlineList(
//                        ""+cursor.getInt(cursor.getColumnIndex(Constants.A_ID)),
//                        ""+cursor.getString(cursor.getColumnIndex(Constants.A_NAME))
//                );
//
//
//                modelRecords.add(airlineList);
//            }while (cursor.moveToNext());
//        }
////        Log.i("airline", modelRecords.get(1).getId().toString());
//        db.close();
//
//        return modelRecords;
//
//    }

    public Cursor getairlinedata()
    {
        SQLiteDatabase db= this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+AIRLINE_TABLE_NAME,null);
        return res;
    }
    public Cursor getflightdata()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res= db.rawQuery("select * from "+FLIGHT_TABLE_NAME,null);
        return res;
    }
    public Cursor getMagazinelistdata()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res= db.rawQuery("select * from "+MAGAZINE_IMG_TABLE_NAME,null);
        return res;
    }

    public Cursor getMagazinepagesdata(String mid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res= db.rawQuery("select * from "+MAGAZINE_PAGES_TABLE_NAME+ " where " + M_ID + " = " + mid ,null);
        return res;
    }


}


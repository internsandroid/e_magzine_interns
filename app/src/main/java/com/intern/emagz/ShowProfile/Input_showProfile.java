package com.intern.emagz.ShowProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Input_showProfile {
    @SerializedName("operation")
    @Expose
    private String operation;
    @SerializedName("api_key")
    @Expose
    private String apiKey;
    @SerializedName("user_id")
    @Expose
    private String userId;

    public String getOperation() {
        return operation;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getUserId() {
        return userId;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}

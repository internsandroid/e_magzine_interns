package com.intern.emagz.ShowProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Output_showProfile {
    @SerializedName("responseStatus")
    @Expose
    private Integer responseStatus;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("User_Info")
    @Expose
    private UserInfo userInfo;

    public Integer getResponseStatus() {
        return responseStatus;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setResponseStatus(Integer responseStatus) {
        this.responseStatus = responseStatus;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
}

package com.intern.emagz.ShowProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UserInfo  {

    @SerializedName("User_Full_Name")
    @Expose
    private String userFullName;
    @SerializedName("Password")
    @Expose
    private String password;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;

    public String getUserFullName() {
        return userFullName;
    }

    public String getPassword() {
        return password;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

package com.intern.emagz.ShowProfile;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.intern.emagz.Featurecontroller;
import com.intern.emagz.R;
import com.intern.emagz.api.Api;
import com.intern.emagz.api.ApiClient;
import com.intern.emagz.update.Input_updateProfile;
import com.intern.emagz.update.Output_updateProfile;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Userdetail extends AppCompatActivity {
    String userid;
    EditText name, email, number, passs ;
    ImageView imageView ;
    byte[] bb = null;
    private Bitmap bm ;
    String base64String = "";
    String img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userdetail);
        userid = Featurecontroller.getInstance().getUserinfo().get(0).getId().toString();
        init();
        showprofile();
    }
    public void init()
    {   imageView = findViewById(R.id.show_image);
        name= findViewById(R.id.show_name);
        name.setEnabled(false);
        email = findViewById(R.id.show_email);
        email.setEnabled(false);
        passs = findViewById(R.id.show_pass);
        passs.setEnabled(false);
        number = findViewById(R.id.show_number);
        number.setEnabled(false);

    }
    public void enable(View v)
    {name.setEnabled(true);
        email.setEnabled(true);
        passs.setEnabled(true);
        number.setEnabled(true);
    }

    public void setdata(Response<Output_showProfile> response) {
        String imgaeeeee =Featurecontroller.getInstance().getUserinfo().get(0).getUserImage();
        name.setText(response.body().getUserInfo().getUserFullName().toString());
        email.setText(response.body().getUserInfo().getEmail().toString());
        passs.setText(response.body().getUserInfo().getPassword().toString());
       // number.setText(response.body().getUserInfo().getCountryCode()+" "+response.body().getUserInfo().getPhone().toString());
        number.setText(response.body().getUserInfo().getPhone().toString());
        Glide.with(getApplicationContext())
                .load("http://testemagazine.startupworld.in/magazine/user_image/user_default_img.png")
                .apply(RequestOptions.circleCropTransform())
                .thumbnail(0.5f)
                .placeholder(R.drawable.userrr)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imageView);

       


    }

    private boolean checkExtPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public void onImageupload(View view) {
        openOptoinDialog();
    }
    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            Toast.makeText(getApplicationContext(), "To access Camera permission, Please allow in App Settings for camera functionality.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode==1)
                onSelectFromGalleryResult(data,1);

            //   else if (requestCode == 1)
            //     onCaptureImageResult(data);
        }
    }

    private void onSelectFromGalleryResult(Intent data, int i) {
        bm = null;
        if (data != null) {
            try {
                Uri imageUri = data.getData();
                InputStream imageStream = getContentResolver().openInputStream(imageUri);
                bm = BitmapFactory.decodeStream(imageStream);
                bm = getResizedBitmap(bm, 400);
                imageView.setImageBitmap(bm);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        getBase64(bm);

    }
    public String getBase64(Bitmap bm1) {
        if (bm1 != null) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            //  bmap.compress(Bitmap.CompressFormat.PNG, 50, bos);
            bm1.compress(Bitmap.CompressFormat.JPEG, 50, bos);
            bb = bos.toByteArray();
            base64String = Base64.encodeToString(bb, Base64.DEFAULT);
        }

        Log.i("Image", base64String);
        return base64String;
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    private void galleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent,1);



    }

    public boolean hasPermissionInManifest(Context context, String permissionName) {
        final String packageName = context.getPackageName();
        try {
            final PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            final String[] declaredPermisisons = packageInfo.requestedPermissions;
            if (declaredPermisisons != null && declaredPermisisons.length > 0) {
                for (String p : declaredPermisisons) {
                    if (p.equals(permissionName)) {
                        return true;
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace(); }
        return false;
    }


    private void openOptoinDialog() {
        final CharSequence[] items = { "Choose from Library", "Cancel"};
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(Userdetail.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Choose from Library")) {
                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onSubmitReq(View view) {
            img=getBase64(bm);
            submitdata();
        }

    private void submitdata() {
        Input_updateProfile i = new Input_updateProfile();
        i.setApiKey("cda11aoip2Ry07CGWmjEqYvPguMZTkBel1V8c3XKIxwA6zQt5s");
        i.setCountryCode("+91");
        i.setOperation("user_profile_update");
        i.setEmail(email.getText().toString());
        i.setFullName(name.getText().toString());
        i.setPassword(passs.getText().toString());
        i.setPhone(number.getText().toString());
        i.setUserId(userid);
        i.setUserImage(img);

        Api api= ApiClient.getClient().create(Api.class);
        Call<Output_updateProfile> call = api.updateuser(i);
        call.enqueue(new Callback<Output_updateProfile>() {
            @Override
            public void onResponse(Call<Output_updateProfile> call, Response<Output_updateProfile> response) {
                if (response.body() != null) {
                    if (response.body().getResponseStatus() == 200) {

                        Toast.makeText(Userdetail.this, "Details updated", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Output_updateProfile> call, Throwable t) {

            }
        });



    }


    public void showprofile() {
        Input_showProfile i = new Input_showProfile();
        i.setUserId(userid.toString());
        i.setApiKey("cda11aoip2Ry07CGWmjEqYvPguMZTkBel1V8c3XKIxwA6zQt5s");
        i.setOperation("user_profile_show");

        Api api = ApiClient.getClient().create(Api.class);
        Call<Output_showProfile> call = api.getUserDetails(i);
        call.enqueue(new Callback<Output_showProfile>() {
            @Override
            public void onResponse(Call<Output_showProfile> call, Response<Output_showProfile> response) {
                if (response.body() != null) {
                    if (response.body().getResponseStatus() == 200) {
                        setdata(response);
                        Toast.makeText(Userdetail.this, "Details get", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Output_showProfile> call, Throwable t) {

            }
        });
    }
}
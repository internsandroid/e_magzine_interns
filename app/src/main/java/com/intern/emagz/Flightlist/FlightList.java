package com.intern.emagz.Flightlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightList {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("airline_code")
    @Expose
    private String airlineCode;
    @SerializedName("flight_code")
    @Expose
    public String flightCode;
    @SerializedName("flight_from")
    @Expose
    private String flightFrom;
    @SerializedName("flight_to")
    @Expose
    private String flightTo;

    public FlightList(String s, String s1) {
    }

    public String getId() {
        return id;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public String getFlightCode() {
        return flightCode;
    }

    public String getFlightFrom() {
        return flightFrom;
    }

    public String getFlightTo() {
        return flightTo;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public void setFlightCode(String flightCode) {
        this.flightCode = flightCode;
    }

    public void setFlightFrom(String flightFrom) {
        this.flightFrom = flightFrom;
    }

    public void setFlightTo(String flightTo) {
        this.flightTo = flightTo;
    }
}

package com.intern.emagz.Flightlist; ;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Input_FlightList {
    @SerializedName("operation")
    @Expose
    private String operation;
    @SerializedName("api_key")
    @Expose
    private String apiKey;
    @SerializedName("airline_code")
    @Expose
    private String airlineCode;

    public String getOperation() {
        return operation;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }
}

package com.intern.emagz;

import android.graphics.Bitmap;

import com.intern.emagz.Button_Action.Coupon_info;
import com.intern.emagz.Button_Action.Output_Button_A_I;
import com.intern.emagz.Button_Action.ProductDetail;
import com.intern.emagz.Login.UserDetail;

import java.util.ArrayList;
import java.util.List;

public class Featurecontroller {

    public  static  Featurecontroller controller;
    List<UserDetail> userDetail ;
    String magid;
    String airlineid;
    String flightid;
    String[] magazineid;
    String[] magdesc;
    String[] magmonth;
    String[] magtitle;
    ArrayList<Bitmap> magazineimg = new ArrayList<>();
    List<ProductDetail> maginfo;
    List<Coupon_info> couponinfo;



    public String getFlightid() {
        return flightid;
    }

    public void setFlightid(String flightid) {
        this.flightid = flightid;
    }

    public String getAirlineid() {
        return airlineid;
    }

    public void setAirlineid(String airlineid) {
        this.airlineid = airlineid;
    }

    public static Featurecontroller getController() {
        return controller;
    }

    public static void setController(Featurecontroller controller) {
        Featurecontroller.controller = controller;
    }

    public List<UserDetail> getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(List<UserDetail> userDetail) {
        this.userDetail = userDetail;
    }

    public String getMagid() {
        return magid;
    }

    public void setMagid(String magid) {
        this.magid = magid;
    }

    public  static  Featurecontroller getInstance(){  //static method return object of type Featurecontroller
                if(controller == null)
                {
                    controller =  new Featurecontroller();
                }
                return  controller;
    }
    public List<UserDetail> getUserinfo()  // Use to get user data
    {
        return  userDetail;
    }
    public  void setUserdetails(List<UserDetail> details) // Use to set user data used in user login
    {
       this.userDetail = details;
    }

    public void setMagazinelist(String[] id , String[] title , String[] desc , String[] month , ArrayList<Bitmap> imgs )
    {
        this.magazineid = id;
        this.magdesc = desc;
        this.magazineimg = imgs;
        this.magtitle = title;
        this.magmonth = month;
    }

    public String[] getMagazineid()
    {
        return magazineid;
    }
    public String[] getMagdesc()
    {
        return magdesc;
    }
    public String[] getMagmonth()
    {
        return magmonth;
    }
    public String[] getMagtitle()
    {
        return magtitle;
    }
    public ArrayList<Bitmap> getMagazineimg()
    {
        return magazineimg;
    }

    public void setMagazineinfo(List<ProductDetail> info)
    {
        this.maginfo = info;
    }
    public void setCouponinfo(List<Coupon_info> info)
    {
        this.couponinfo = info;
    }


}




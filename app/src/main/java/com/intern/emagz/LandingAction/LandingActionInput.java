package com.intern.emagz.LandingAction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LandingActionInput {
    @SerializedName("operation")
    @Expose
    private String operation;
    @SerializedName("api_key")
    @Expose
    private String apiKey;
    @SerializedName("passanger_id")
    @Expose
    private String passangerId;
    @SerializedName("magazine_id")
    @Expose
    private String magazineId;
    @SerializedName("page_id")
    @Expose
    private String pageId;
    @SerializedName("airline_id")
    @Expose
    private String airlineId;
    @SerializedName("flight_id")
    @Expose
    private String flightId;
    @SerializedName("like")
    @Expose
    private String like;
    @SerializedName("info")
    @Expose
    private String info;
    @SerializedName("coupons_request")
    @Expose
    private String couponsRequest;
    @SerializedName("share")
    @Expose
    private String share;
    @SerializedName("feedback")
    @Expose
    private String feedback;
    @SerializedName("user_feedback")
    @Expose
    private String userFeedback;
    @SerializedName("registration")
    @Expose
    private String registration;
    @SerializedName("chat")
    @Expose
    private String chat;
    @SerializedName("callback")
    @Expose
    private String callback;
    @SerializedName("device_name")
    @Expose
    private String deviceName;
    @SerializedName("device_os_version")
    @Expose
    private String deviceOsVersion;
    @SerializedName("ip_address")
    @Expose
    private String ipAddress;
    @SerializedName("source")
    @Expose
    private String source;

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getPassangerId() {
        return passangerId;
    }

    public void setPassangerId(String passangerId) {
        this.passangerId = passangerId;
    }

    public String getMagazineId() {
        return magazineId;
    }

    public void setMagazineId(String magazineId) {
        this.magazineId = magazineId;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getAirlineId() {
        return airlineId;
    }

    public void setAirlineId(String airlineId) {
        this.airlineId = airlineId;
    }

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getCouponsRequest() {
        return couponsRequest;
    }

    public void setCouponsRequest(String couponsRequest) {
        this.couponsRequest = couponsRequest;
    }

    public String getShare() {
        return share;
    }

    public void setShare(String share) {
        this.share = share;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getUserFeedback() {
        return userFeedback;
    }

    public void setUserFeedback(String userFeedback) {
        this.userFeedback = userFeedback;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getChat() {
        return chat;
    }

    public void setChat(String chat) {
        this.chat = chat;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceOsVersion() {
        return deviceOsVersion;
    }

    public void setDeviceOsVersion(String deviceOsVersion) {
        this.deviceOsVersion = deviceOsVersion;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

}

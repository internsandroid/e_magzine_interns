package com.intern.emagz;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import static com.intern.emagz.R.layout.messagedialog;

public class Adapter_show_pages extends RecyclerView.Adapter<Adapter_show_pages.myViewholder> {
    private  String[] image;
    private  Context context;
    private  ArrayList<Bitmap> pageimgs;
    private int bool =0;
    public Adapter_show_pages(String[] pageimg, Context context) {
        bool =0;
        this.image = pageimg;
        this.context = context;
    }
    public Adapter_show_pages(ArrayList<Bitmap> pageimgs , Context context){
        bool = 1;
        this.context = context;
        this.pageimgs = pageimgs;
    }

    @NonNull
    @Override
    public myViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.page,parent,false);
        return new myViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull myViewholder holder, final int position){

                holder.bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.likes:
                        CommonFuntion.liked(context,String.valueOf(position+1),CommonFuntion.imgtobase64(image[position],context));
                        break;
                    case R.id.coupon:
                        CommonFuntion.coupon(context,String.valueOf(position+1),CommonFuntion.imgtobase64(image[position],context));
                        break;
                    case R.id.share:
                       // Toast.makeText(context,"shared clicked",Toast.LENGTH_SHORT).show();
                        StrictMode.VmPolicy.Builder builder=new StrictMode.VmPolicy.Builder();
                        StrictMode.setVmPolicy(builder.build());
                        Picasso.with(context).load(image[position]).into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setType("image/jpg");
                                i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap));
                                /*i.putExtra(Intent.EXTRA_TEXT, blogModelList.get(getAdapterPosition()).getBlogTitle() +":"+ "\n"
                                        + blogModelList.get(getAdapterPosition()).getDescription() + "\n"
                                        + blogModelList.get(getAdapterPosition()).getDate());*/
                                context.startActivity(Intent.createChooser(i,null));
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                            }
                        });
                        break;
                    case R.id.info:
                        CommonFuntion.info(context,String.valueOf(position));
                        break;
                    case R.id.message:
                        //Toast.makeText(context,"Message clicked",Toast.LENGTH_SHORT).show();

                        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
                        LayoutInflater inflater = LayoutInflater.from(context);
                        View mView = inflater.inflate(messagedialog,null);
                        final EditText txt_inputText = (EditText)mView.findViewById(R.id.message);
                        Button btn_cancel = (Button)mView.findViewById(R.id.cancel);
                        Button btn_okay = (Button)mView.findViewById(R.id.submit);
                        alert.setView(mView);
                        final AlertDialog alertDialog = alert.create();
                        alertDialog.setCanceledOnTouchOutside(false);
                        btn_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                            }
                        });
                        btn_okay.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                CommonFuntion.feedback(context,txt_inputText.getText().toString(),String.valueOf(position),CommonFuntion.imgtobase64(image[position],context));
                                Log.i("Message",txt_inputText.getText().toString());
                                alertDialog.dismiss();
                            }
                        });
                        alertDialog.show();
                        break;
                }
                return false;
            }
        });
                if(bool==0)
                {
                    Glide.with(context)
                            .load(image[position])
                            .placeholder(R.drawable.loading)
                            .into(holder.imageView);
                }
                else
                {
                    holder.imageView.setImageBitmap(pageimgs.get(position));
                }

    }

    public Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "" + System.currentTimeMillis() + ".jpg");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    @Override
    public int getItemCount() {
           if(bool==0)
         {
            return  image.length;
         }
          else {
          return  pageimgs.size();
               }
    }

    public class myViewholder extends RecyclerView.ViewHolder
    {
        ImageView imageView;
        BottomNavigationView bottomNavigationView;
        public myViewholder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.page);
            bottomNavigationView = itemView.findViewById(R.id.Navigation);


        }
    }
}

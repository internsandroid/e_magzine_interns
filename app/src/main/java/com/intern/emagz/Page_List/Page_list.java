package com.intern.emagz.Page_List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Page_list {
    @SerializedName("Magazine_Page_Number")
    @Expose
    private String magazinePageNumber;
    @SerializedName("Magazine_page_image")
    @Expose
    private String magazinePageImage;
    @SerializedName("like")
    @Expose
    private String like;
    @SerializedName("info")
    @Expose
    private String info;
    @SerializedName("coupons")
    @Expose
    private String coupons;
    @SerializedName("share")
    @Expose
    private String share;
    @SerializedName("feedback")
    @Expose
    private String feedback;
    @SerializedName("registration")
    @Expose
    private String registration;
    @SerializedName("chat")
    @Expose
    private String chat;
    @SerializedName("callback")
    @Expose
    private String callback;

    public String getMagazinePageNumber() {
        return magazinePageNumber;
    }

    public String getMagazinePageImage() {
        return magazinePageImage;
    }

    public String getLike() {
        return like;
    }

    public String getInfo() {
        return info;
    }

    public String getCoupons() {
        return coupons;
    }

    public String getShare() {
        return share;
    }

    public String getFeedback() {
        return feedback;
    }

    public String getRegistration() {
        return registration;
    }

    public String getChat() {
        return chat;
    }

    public String getCallback() {
        return callback;
    }

    public void setMagazinePageNumber(String magazinePageNumber) {
        this.magazinePageNumber = magazinePageNumber;
    }

    public void setMagazinePageImage(String magazinePageImage) {
        this.magazinePageImage = magazinePageImage;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setCoupons(String coupons) {
        this.coupons = coupons;
    }

    public void setShare(String share) {
        this.share = share;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public void setChat(String chat) {
        this.chat = chat;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

}

package com.intern.emagz.Page_List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Input_page_L {
    @SerializedName("operation")
    @Expose
    private String operation;
    @SerializedName("api_key")
    @Expose
    private String apiKey;
    @SerializedName("Magazine_id")
    @Expose
    private String magazineId;

    public String getOperation() {
        return operation;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getMagazineId() {
        return magazineId;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public void setMagazineId(String magazineId) {
        this.magazineId = magazineId;
    }

}
